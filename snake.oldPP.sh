#!/bin/env php
<?php
function main()
{
    $win; // Window instance
    $size = array(); // Y & X => Height & Width

    checkNcurse(); // ncurse is loaded ?
    createWindow($win, $size);
    mainLoop($win, $size);
    closeWindow();
}

function mainLoop(&$win, &$size)
{
    while (1) {
        $usedKey = getStdin();

        if (event($usedKey, $win)) {
            break;
        }

        printDraw($win ,$size);

        usleep(100*1000);
    }
}

function printDraw(&$win ,&$size)
{
    // ! CLEAR :
    ncurses_wclear($win);

    ncurses_wrefresh($win);
    ncurses_getmaxyx($win, $size['height'], $size['width']);

    ncurses_border(0,0,  0,0,  0,0,  0,0);
    // : CLEAR !

    // ! PRINT :
    snake($win);
    // : PRINT !

    // ! REFRESH !
    ncurses_wmove($win, 0, 0); // Get out cursor
    ncurses_wrefresh($win);
}

function snake(&$win, $mX = 0, $mY = 0, $fatter = false)
{
    static $size  = 1;
    static $x, $y = 1;
    static $smX   = 0;
    static $smY   = 0;

    if ($fatter) {
        ++$size;
    }

    if ($mX || $mY) {
        $x = $x + $mX;
        $y = $y + $mY;

        $smX = $mX;
        $smY = $mY;
    } else {
        snakeMove($x, $y, $smX, $smY);
    }

    ncurses_wattron($win, NCURSES_A_REVERSE);
    ncurses_wmove($win, $y, $x);
    ncurses_waddstr($win, ' ');
    ncurses_wattroff($win, NCURSES_A_REVERSE);
}

function snakeMove(&$x, &$y, &$X, &$Y)
{
    $x = $x + $X;
    $y = $y + $Y;
}

function event(&$usedKey, &$win)
{
    switch ($usedKey) {
        case 'q':
        case 'Q':
            return (true);
            break;
        case '8':
            snake($win, 0, -1);
            break;
        case '2':
            snake($win, 0, 1);
            break;
        case '4':
            snake($win, -1, 0);
            break;
        case '6':
            snake($win, 1, 0);
            break;
    }

    return (false);
}

function getStdin()
{
        $file = fopen('php://stdin', 'r');
		stream_set_blocking($file, false);
		$key = fread($file, 1);

        var_dump($key);
        return ($key);
}

function checkNcurse()
{
    if (!extension_loaded('ncurses')) {
        exit('DIE : The ncurses lib is not loaded.');
    }
}

function createWindow(&$win, &$size)
{
    ncurses_init();
    ncurses_curs_set(0); // Disable cursor

    if ($win = ncurses_newwin(0, 0, 0, 0)) {
        ncurses_refresh();
        ncurses_getmaxyx(STDSCR, $size['height'], $size['width']);

        ncurses_noecho(); // No print used keys
        //ncurses_keypad($win, true); // Return keypad keys
    } else {
        closeWindow();
        exit('ERR : ncurses_newwin => FAIL !');
    }
}

function closeWindow()
{
    ncurses_end();
}

main();
